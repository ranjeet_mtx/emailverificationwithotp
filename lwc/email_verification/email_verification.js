/**
 * @description JS Controller for "Email Verification with OTP".
 * @author Ranjeet 
 * @version 1.0
 */
import { LightningElement, track, api } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import sendOTP from '@salesforce/apex/EmailVerificationController.sendOTP';

export default class email_verification extends LightningElement {
    @track isVerified;
    @track userInputCode;
    @track expectedOtp;
    @track fullName;
    @track emailId;

    connectedCallback() {
        this.isVerified = false;
        this.generateAndSendOtp();
    }

    generateAndSendOtp() {
        if(this.fullName && this.emailId) {
            sendOTP({
                name: this.fullName,
                emailAddress: this.emailId
            })
            .then( verificationCode => {
                this.expectedOtp = verificationCode;
            })
            .catch( error => {
                console.log("ERROR");
                console.log(JSON.stringify(error));
                this.showErrorMessageUsingToast(null, error);
            })
        }
    }

    handleSendOtpRequest() {
        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
        if (allValid) {
            this.expectedOtp = undefined;
            this.generateAndSendOtp();
        }
    }

    setFullName(event) {
        let fullNameValue = event.detail.value;
        this.fullName = fullNameValue;
    }

    setEmailId(event) {
        let emailIdValue = event.detail.value;
        this.emailId = emailIdValue;
    }

    setUserInputCode(event) {
        this.userInputCode = event.detail.value;
    }

    handleNextButtonAction() {
        const allValid = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputCmp) => {
                        inputCmp.reportValidity();
                        return validSoFar && inputCmp.checkValidity();
            }, true);
        if (allValid) {
            if(this.userInputCode == this.expectedOtp) {
                this.isVerified = true;
                // Verification is successful.
                // If required, fire a Custom Event and handle in Parent Component.
            }
            else {
                this.showErrorMessageUsingToast("OTP is incorrect !");
            }
        }
    }

    /**
     * **********
     * UTILITIES
     * **********
     */
    showSuccessMessageUsingToast(message) {
        if(! message) {
            message = "Your request has been processed successfully !"
        }
        this.dispatchEvent(new ShowToastEvent({
            title: 'Success',
            message: message,
            variant: 'success'
        }));
    }

    showErrorMessageUsingToast(message, auraHandledExceptionObject) {
        if(! message) {
            message = "An unexpected error had occured ! Please contact your Salesforce Admin !"
        }
        if(auraHandledExceptionObject && auraHandledExceptionObject.body) {
            message = auraHandledExceptionObject.body.message;
        }
        this.dispatchEvent(new ShowToastEvent({
            title: 'Error',
            message: message,
            variant: 'error'
        }));
    }

    /**
     * **********
     * GETTERS
     * **********
     */

    get disableInputFields() {
        if(this.expectedOtp) {
            return true;
        }
        return false;
    }

    get isOtpRequired() {
        if(this.expectedOtp) {
            return true;
        }
        return false;
    }

    get sendOtpButtonLabel() {
        if(this.expectedOtp) {
            return 'Resend OTP';
        }
        return 'Send OTP';
    }

    get disableSendOtpButton() {
        if(this.fullName && this.emailId) {
            return false;
        }
        return true;
    }
    
    get disableNextButton() {
        if(this.userInputCode) {
            return false;
        }
        return true;
    }
}