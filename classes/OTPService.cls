/**
 * @description This class will manage all OTP related Logics.
 * @author Ranjeet
 */
public with sharing class OTPService {
    
    public class OTPServiceException extends Exception {}
	
    @TestVisible
    private static Integer OTP_LENGTH;
    @TestVisible
    private static Integer DEFAULT_OTP_LENGTH = 6;
    @TestVisible
    private Integer getOtpLength() {
        if(OTP_LENGTH == null || OTP_LENGTH < 1) {
            OTP_LENGTH = DEFAULT_OTP_LENGTH;
        }
        return OTP_LENGTH;
    }
    
    public void setOtpLength(Integer otpLength) {
        if(otpLength != null && otpLength > 0) {
            OTP_LENGTH = otpLength;
        }
    }
    
    @TestVisible
    private String generateOtp() {
        Integer multiplier = (Integer) Math.pow(10, this.getOtpLength());
        Integer randomInteger = Math.round(Math.random() * multiplier);
        if(randomInteger == 0) {
            randomInteger = Math.round(Math.random() * multiplier) + 1;
        }
        String new_otp = String.valueOf(randomInteger);
        if(new_otp.length() < this.getOtpLength()) {
            new_otp = new_otp.rightPad(this.getOtpLength(), '5');
        }
        return new_otp;
    }
    
    /**
     * ********************
     *  METHODS TO GET OTP
     * ********************
     */
    /**
     * @description Will generte and return OTP of defult length.
     * @param none
     * @return String OTP
     */
    public String getOtp() {
        return getOtp(DEFAULT_OTP_LENGTH);
    }
    
    /**
     * @description Will generte and return OTP.
     * @param otpLength Length of OTP. Defualt is 6 digit
     * @return String OTP
     */
    public String getOtp(Integer otpLength) {
        try {
            this.setOtpLength(otpLength);
        	return generateOtp();
        } catch(Exception e) {
            System.debug(e.getStackTraceString());
            throw new OTPServiceException(e.getMessage());
        }
    }
}