/**
 * @description Used for sending Email with verification code.
 * @author Ranjeet
 */
public class EmailVerificationController {
    
    /**
     * @description Will generate, send and return the OTP.
     * @param name Name to be used in email.
     * @param emailId To which email with verification code to be sent.
     * @return OTP OTP which will be send in email.
     */
	@AuraEnabled
    public static String sendOTP(String name, String emailAddress) {
        try {
            // Generate OTP
            OTPService otp_service_instance = new OTPService();
            String verificationCode  = otp_service_instance.getOtp(6);
            
            // Create email with verification code
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            string[] toEmails = new string[] {emailAddress};
            email.setToAddresses(toEmails);
            email.setSubject('Email Verification Code');
            String emailBody = '';
            emailBody += 'Hi ' + name + ', ';
            emailBody += '<br/><br/>';
            emailBody += 'Your verification code is following : ';
            emailBody += '<br/><br/>';
            emailBody += verificationCode;
            emailBody += '<br/><br/>';
            emailBody += 'Thanks';
            email.setHtmlBody(emailBody);
            
            // Send Email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            return verificationCode;
        } catch(exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }
}